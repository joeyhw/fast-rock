# fast-rock
---
Faster User-Agent to TAL device identification.  
Based on [bbc/melanite](https://www.npmjs.com/package/melanite)

For example usage see example.js

**this package uses [neon](https://neon-bindings.com/docs/getting-started) for [rust](https://www.rust-lang.org/)**  

## API

See: [https://joeyt.co.uk/fast/api/](https://joeyt.co.uk/fast/api/)


Matcher:
```
{
  brand: String, // the brand of the device
  model: String, // the model name of the device
  invariants: [String], // substrings that will always be present
  disallowed: [String], // substrings that can never be present
  fuzzy: String // an example user agent that will be fuzzily matched, assuming that the invariants and disalloweds are met
}
```

Device:
```
{
  brand: String, // the brand of the device
  model: String, // the model name of the device
}
```
