var native = require('../native');

/**
 * @module fastrock 
 */

/**
 * Fast rock device identifier
 */
class FastRock {
  /**
   * Creates a FastRock
   * @param {Matcher[]} matchers 
   */
  constructor(matchers) {
    this.matcher = new native.Matchers({
      matchers,
      default: {
        brand: 'generic',
        model: 'device',
        invariants: [],
        disallowed: [],
        fuzzy: 'GENERIC DEVICE'
      }
    })
  }

  /**
   * Matches a user agent string to a Device
   * @param {String} userAgent a web browser user agent string
   * @returns {Device} the device that the user agent belongs to
   */
  match_device(userAgent) {
    return this.matcher.match_device(userAgent)
  }
}
module.exports = FastRock


/**
 * Device data object
 * @typedef {Object} Device
 * @property {string} brand The brand of device
 * @property {string} model The model of device
 * @property {string} ua The user agent string used to match the device
 */

/**
* Matcher data object
* @typedef {Object} Matcher
* @property {string} brand The brand of device
* @property {string} model The model of device
* @property {string[]} invariants substrings that must be present in order to match
* @property {string[]} disallowed substrings that must not be present in order to match
* @property {string} fuzzy An example user agent that will be fuzzily matched, assuming that the invariants and disalloweds are met
*/
