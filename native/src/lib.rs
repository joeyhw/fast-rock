extern crate neon;
extern crate neon_serde;
extern crate serde;
extern crate serde_derive;
extern crate edit_distance;
extern crate regex;
use std::borrow::Cow;

use regex::Regex;
use neon::{register_module, declare_types, class_definition, impl_managed, prelude::*};
use serde_derive::{Serialize, Deserialize};
use edit_distance::edit_distance;

#[derive(Clone, Debug)]
pub struct Matcher {
  brand: Cow<'static, str>,
  model: Cow<'static, str>,
  invariants: Vec<Cow<'static, str>>,
  disallowed: Vec<Cow<'static, str>>,
  invariants_regex: Vec<Regex>,
  disallowed_regex: Vec<Regex>,
  fuzzy: Cow<'static, str>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct MatcherData {
  brand: Cow<'static, str>,
  model: Cow<'static, str>,
  invariants: Vec<Cow<'static, str>>,
  disallowed: Vec<Cow<'static, str>>,
  fuzzy: Cow<'static, str>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct BrandModel {
  brand: Cow<'static, str>,
  model: Cow<'static, str>
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct MatchersData {
  matchers: Vec<MatcherData>,
  default: MatcherData
}

#[derive(Clone, Debug)]
pub struct Matchers {
  matchers: Vec<Matcher>,
  default: Matcher
}

impl Into<Matchers> for MatchersData {
  fn into(self) -> Matchers {
    Matchers {
      default: self.default.into(),
      matchers: self.matchers.iter().map(|matcher| {
        matcher.clone().into()
      }).collect()
    }
  }
}

impl Into<Matcher> for MatcherData {
  fn into(self) -> Matcher {
    Matcher {
      brand: self.brand,
      model: self.model,
      invariants_regex: self.invariants.iter().map(|invariant| {
        Regex::new(&regex::escape(&invariant)).unwrap()
      }).collect(),
      disallowed_regex: self.disallowed.iter().map(|disallowed| {
        Regex::new(&regex::escape(&disallowed)).unwrap()
      }).collect(),
      invariants: self.invariants,
      disallowed: self.disallowed,
      fuzzy: self.fuzzy
    }
  }
}

impl From<Matcher> for MatcherData {
  fn from(matcher: Matcher) -> MatcherData {
    MatcherData {
      brand: matcher.brand,
      model: matcher.model,
      invariants: matcher.invariants,
      disallowed: matcher.disallowed,
      fuzzy: matcher.fuzzy
    }
  }
}

declare_types! {
  pub class JsMatchers for Matchers {
    init(mut cx) {
      let arg0 = cx.argument::<JsValue>(0)?;
      let matchers: MatchersData = neon_serde::from_value(&mut cx, arg0)?;

      Ok(matchers.into())
    }
    
    method match_device(mut cx) {
      let to_match: String = cx.argument::<JsString>(0)?.value();
      let best_match: BrandModel = {
        let this = cx.this();
        let guard = cx.lock();
        let matchers = this.borrow(&guard);
        let mut best_match: &Matcher = &matchers.default;
        let mut smallest_distance = std::usize::MAX;

        let matchers = &matchers.matchers;
        for matcher in matchers {
          let has_no_disallowed: bool = matcher.disallowed_regex.iter().all(|disallowed| !disallowed.is_match(&to_match));

          if has_no_disallowed {
            let has_invariants: bool = matcher.invariants_regex.iter().all(|invariant| invariant.is_match(&to_match));
            if has_invariants {
              let distance = edit_distance(&matcher.fuzzy, &to_match);
              if distance < smallest_distance {
                smallest_distance = distance;
                best_match = matcher;
              }
            }
          }
        }

        BrandModel {
          brand: best_match.brand.clone(),
          model: best_match.model.clone()
        }
      };

      let js_best_match = neon_serde::to_value(&mut cx, &best_match)?;
      Ok(js_best_match)
    }
  }
}

register_module!(mut m, { m.export_class::<JsMatchers>("Matchers") });
